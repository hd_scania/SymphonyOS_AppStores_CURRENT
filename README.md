# SymphonyOS’s personal App Stores (WiP)
App Stores = My personal and unofficial FreeBSD port tree pools

![My logo~](./os_SymphonyOS.AppStores.png)
## Contained apps
1. [Liri shell](https://liri.io), a Wayland only Qt5 desktop composition
2. [Trinity TDE](https://TrinityDesktop.org), an independent KDE 3 fork
3. [Weston](https://wayland.freedesktop.org/building.html), the primary Wayland composition
4. [Kotatogram](https://kotatogram.GitHub.io), an unofficial Telegram desktop app
5. [Brave web browser](https://brave.com), an independent privacy oriented web browser
## Git mirrors
### CURRENT
1. [`https://git.code.sf.net/p/symphony-freebsd/stableappstores/CURRENT`](https://sf.net/p/symphony-freebsd/stableappstores/CURRENT) (Primary SourceForge trees)
2. https://notabug.org/HD_Scanius/SymphonyOS_AppStores_CURRENT (Not-a-bug mirrors)
3. https://GitLab.com/hd_scania/SymphonyOS_AppStores_CURRENT (GitLab mirrors)
### 16-STABLE, 15-STABLE, and 14-STABLE
Coming soon when those FreeBSD `-STABLE` releases are ready
### 13-STABLE
1. [`https://git.code.sf.net/p/symphony-freebsd/stableappstores/13-STABLE`](https://sf.net/p/symphony-freebsd/stableappstores/13-STABLE) (Primary SourceForge trees)
2. https://notabug.org/HD_Scanius/SymphonyOS_AppStores_13-STABLE (Not-a-bug mirrors)
3. https://GitLab.com/hd_scania/SymphonyOS_AppStores_13-STABLE (Not-a-bug mirrors)
## Minimum OS requirements
* FreeBSD `13-STABLE` and up
## Minimum hardware requirements
* AMD Ryzen 4600U **6C12T** or higher, or equivalent Intel replacements
* AMD Radeon ‘‘Vega 7’’ and up (Ryzen APU’s also count in) or NVidia 390+
* Running Liri shell is not supported on NVidia cards due to a `nouveau` requirement FreeBSD misses
